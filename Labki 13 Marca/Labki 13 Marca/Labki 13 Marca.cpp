// Labki 13 Marca.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "math.h"

#define MAX_ITERATIONS 50

double calucalte_error(double result_value, double exact_value);
double exponent(double x, double eps);
void read_values(double *argument, double *eps);
void printf_single_result(int it_count,double result, double error);

int main() {
	double n = 0, eps = 0;
	read_values(&n, &eps);
	double result = exponent(n, eps);
	getchar();
	return 0;
}

double calucalte_error(double result_value, double exact_value) {
	double nominator = fabs(result_value - exact_value);
	double denominator = fabs(exact_value);
	return nominator / denominator;
}

double exponent(double x, double eps) {
	double current_sum = 1;
	double previous_result = 1;
	double cached_result = exp(x);
	//Helpers for calculating factorial and power
	int i = 1;
	//Cache for an error to be printed
	double error = 0;

	do {
		previous_result *= x / i;
		current_sum += previous_result;
		error = calucalte_error(current_sum, cached_result);

		printf_single_result(i, current_sum, error);

		i++;
	} while (i < MAX_ITERATIONS && eps < error);

	return current_sum ;
}

/*
IO -> To be moved
*/

void read_values(double *argument, double *eps) {
	scanf_s("%lf %lf", argument, eps);
}

void printf_single_result(int it_count, double result, double error) {
	printf_s("N=%d,Result=%lf,Error=%lf \n",it_count, result, error);
}
