
#include "stdafx.h"
#include "MyInterf.h"
#include <stdlib.h>
#include <string.h>

void MyMenu()
{
	int ops;
	MY_ARRAY_ELEM *Array = CreateArray(5);

	while (1)
	{
		printf("0 - add object, 1 - print all objects, 2 - exit\n");
		scanf_s("%d", &ops);

		switch (ops)
		{
		case 0:
			AddObject(Array);
			break;
		case 1:
			PrintAllObjects(Array);
			break;
		case 2:
			MyExit(Array);
			break;
		default:
			printf("unknown operation\n");

		};
	}
}

void AddObject(MY_ARRAY_ELEM * arr)
{
	int typ;
	Triangle *tr = NULL;
	MY_ARRAY_ELEM tmp;

	printf("enter typ: 0 - triangle, 1 - quadrilateral, 2 - circle\n");
	scanf_s("%d", &typ);

	switch (typ)
	{
	case 0: 
		tr = (Triangle *)malloc(sizeof(Triangle));  //powstaje obiekt Triangle
		if (!tr)
		{
			printf("memory allocation error\n");
			return;
		}
		TriangInit(tr);
		TriangInput(tr);

		tmp.ptr_data     = (void *)tr;
		tmp.typ          = MY_DATA_TYPE_TRIANG;
		tmp.ptr_fun_prnt = TriangPrint;
		tmp.ptr_fun_free = TriangFree;

		if (!PushObject(arr, &tmp))
		{
			MyInterfDealloc(tr);
			printf("Push object error\n");
		}
		break;

	case 1:
		break;

	case 2:
		break;

	default:
		printf("unknown type of object\n");
	};
}

void PrintAllObjects(MY_ARRAY_ELEM * arr)
{
	PrintArray(arr);
}

void MyExit(MY_ARRAY_ELEM * arr)
{
	printf("exit\n");
	FreeArray(&arr);
	system("pause");
	exit(1);
}

void MyInterfDealloc(void * ptr)
{
	if (ptr)
		free(ptr);
}


