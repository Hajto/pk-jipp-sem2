﻿#include "stdafx.h"
#include "MyTriangle.h"
#include <stdlib.h>
#include <string.h>


void TriangInit(Triangle *ptr)
{
	if (ptr)
	{
		for (int it = 0; it < 3; ++it)
			ptr->vert[it].crd = NULL;
	}
}


void TriangPutData(Triangle *ptr, char *title, MY_POINT *v1, MY_POINT *v2, MY_POINT v3)
/*======================================================================================
IN: ptr - wskaznik do obiekta Triangle
    title - nazwa trojkata
	v1, v2 - wskazniki do pierwszych dwoch wierzcholkow trojkata
	v3    - obiekt trzeciego wierzcholka trojkata
=======================================================================================*/
{
	strcpy_s(ptr->str, sizeof(ptr->str), title);  //OK

	//ptr->vert[0] = *v1;  //!OK
	//ptr->vert[1] = *v2;  //!OK
	//ptr->vert[2] =  v3;  //!OK

	MY_POINT_Equel(&ptr->vert[0], v1);  //OK
	MY_POINT_Equel(&ptr->vert[1], v2);  //OK
	MY_POINT_Equel(&ptr->vert[2], &v3); //OK

	//wierzcholki v1, v2, v3 powstale dynamicznie, za pomoca malloc
	//zwalniamy te obiekty, poniewaz onie dalej nie sa potrzebne!!!
	MY_POINT_Free(&v1);
	MY_POINT_Free(&v2);

	//MY_POINT *pptr = &v3;
	//MY_POINT_Free(&pptr);  !OK - v3 - to jest kopia obiektu w stosie funkcji, nie znajduje się w heap!
	MY_POINT_FreeCoord(&v3);
}

void TriangInput(Triangle *ptr)
{
	printf("enter title and three vertexes in format x y\n");
	char str[64];
	MY_POINT *v1, *v2, *v3;
	double x, y;
	scanf_s("%s", str, sizeof(str));

	scanf_s("%lf%lf", &x, &y);
	v1 = CreateVertex(x, y); 

	scanf_s("%lf%lf", &x, &y);
	v2 = CreateVertex(x, y);

	scanf_s("%lf%lf", &x, &y);
	v3 = CreateVertex(x, y);	

	TriangPutData(ptr, str, v1, v2, *v3);
}

void TriangPrint(void * pdata)
{
	Triangle *ptr = (Triangle *)pdata;

	printf("Triangle %s\n", ptr->str);

	for (size_t it = 0; it < 3; ++it)
	{
		printf("Vertex 1: ");
		PrintVertex(ptr->vert[it]);
	}
}

void TriangFree(void **pTr)
{
	if (pTr)
	{
		if (*pTr)
		{
			Triangle *ptr = (Triangle *)(*pTr);
			//Tablica wierzcholkow vert jest zadeklarowana w sposob statyczny, dla tego
			//free(ptr->vert) bedzie powodowalo Page Fault oraz
			//MY_POINT *pp = &ptr->vert[i]; MY_POINT_Free(&pp) tez bedzie powodowalo Page Fault. 

			//zwalniamy pamiec dla kazdego wierzcholku
			for (int it = 0; it < 3; ++it)
				MY_POINT_FreeCoord(&ptr->vert[it]);

			//Obiekt Triangle powstal dynamicznie, wiec trzeba zwolnic pamiec:
			free(*pTr);
			*pTr = NULL;
		}
	}
}




