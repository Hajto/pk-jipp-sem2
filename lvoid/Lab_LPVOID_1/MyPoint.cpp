#include "stdafx.h"
#include "MyPoint.h"
#include <stdlib.h>
#include <string.h>

void PrintVertex(MY_POINT vert)
{
	if(vert.crd)
		printf("x = %le   y = %le\n", vert.crd[0], vert.crd[1]);
}

MY_POINT *CreateVertex(double x, double y)
{
	MY_POINT *ptr = (MY_POINT *)malloc(sizeof(MY_POINT));
	if (ptr)
	{
		ptr->crd = (double *)malloc(2 * sizeof(double));
		if (!ptr->crd)
		{
			//...
		}

		ptr->crd[0] = x;
		ptr->crd[1] = y;
	}

	return ptr;
}

void MY_POINT_Free(MY_POINT **ob)
/*=======================================================================
Stosujemy ta funkcje dla vierszcholku, ktory powstaje dynamicznie:
MY_POINT *ptr = (MY_POINT *)malloc(...)
=========================================================================*/
{
	if (ob)
	{
		if (*ob)
		{
			if ((*ob)->crd)
				free((*ob)->crd);
			(*ob)->crd = NULL;

			free(*ob);
		}

		*ob = NULL;
	}
}

void MY_POINT_FreeCoord(MY_POINT *ob)
/*=======================================================================
Stosujemy ta funkcje dla vierzcholku, ktory powstaje statycznie:
MY_POINT v; Pamiec dla v.crd alokuje sie po tworzeniu wierzcholku.
=========================================================================*/
{
	if (ob)
	{
		if (ob->crd)
			free(ob->crd);
		ob->crd = NULL;
	}
}


int MY_POINT_Equel(MY_POINT *dest, MY_POINT *source)
/*====================================================
MY_POINT zawiera wskaznik double *crd.
Dla tego 
a = b, gdzie a, b sa obiektami MY_POINT - blad!
To przepisanie powinna wykonac podana funkcja.

RetVal: 1 - OK, 0 - !OK
======================================================*/
{
	if (dest && source && source->crd)
	{
		if (!dest->crd)
			dest->crd = (double *)malloc(2 * sizeof(double));

		if (!dest->crd)
			return 0;

		for (int it = 0; it < 2; ++it)
			dest->crd[it] = source->crd[it];
		return 1;
	}

	return 0;
}
