#pragma once

#include "MyArray.h"
#include "MyTriangle.h"

void MyMenu();
void AddObject(MY_ARRAY_ELEM * arr);
void PrintAllObjects(MY_ARRAY_ELEM * arr);
void MyExit(MY_ARRAY_ELEM * arr);
void MyInterfDealloc(void * ptr);


