#pragma once

struct MY_POINT
{
	double *crd;  //crd[0] - x, crd[1] - y
};

MY_POINT *CreateVertex(double x, double y);
void PrintVertex(MY_POINT vert);
void MY_POINT_Free(MY_POINT **ob);
void MY_POINT_FreeCoord(MY_POINT *ob);
int MY_POINT_Equel(MY_POINT *dest, MY_POINT *source);
