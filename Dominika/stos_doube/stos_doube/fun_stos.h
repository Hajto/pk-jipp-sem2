#pragma once

struct NODE
{
	double wartosc;
	NODE* next;
};

NODE * alloc();

NODE * dealloc(NODE * node);

NODE * NewElem(NODE * previous, int new_elem_val);

NODE * Pop(NODE * node);
