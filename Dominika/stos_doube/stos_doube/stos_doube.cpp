// stos_doube.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "fun_stos.h"


int main()
{
	//Uwagi
	//Z jakiego formantu(%f,%d itd) korzystamy w printfie zeby wypisac wartosc typu double?

	NODE* tmp_stos = NewElem(NULL, 2);
	tmp_stos = NewElem(tmp_stos, 4);
	tmp_stos = NewElem(tmp_stos, 6);
	tmp_stos = NewElem(tmp_stos, 8);
	tmp_stos = NewElem(tmp_stos, 10);
	tmp_stos = NewElem(tmp_stos, 12);
	printf("%d\n\n", tmp_stos); //Wypisujesz wskaznik na pierwszy element stosu? Dlaczego?

	tmp_stos = Pop(tmp_stos);
	printf("%d", tmp_stos); //Wypisujesz wskaznik na pierwszy element stosu? Dlaczego?

	dealloc(tmp_stos);
    return 0;
}

