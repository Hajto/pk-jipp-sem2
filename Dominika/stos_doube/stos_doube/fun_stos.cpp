#include "stdafx.h"
#include "fun_stos.h"
#include <stdlib.h>

NODE* alloc()
{
	NODE* node = (NODE*)malloc(sizeof(NODE));
	if (!node)
	{
		exit(1);
	}

	return node;
}

NODE* dealloc(NODE* node)
{
	NODE* next=NULL;
	while (node !=NULL)
	{
		next = node->next;
		if (node)
		{
			free(node);
		}
		node = next;
	}
	return node;
}

NODE * NewElem(NODE* previous, int new_elem_val)
{
	NODE * node = alloc();
	node->wartosc = new_elem_val;
	node->next = previous;
	return node;
}

NODE * Pop(NODE* node)
{
	NODE* next = node->next;
	free(node);
	return next;
}