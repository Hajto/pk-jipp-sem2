#pragma once
#include "MyPoint.h"

struct Circle
{
	char str[64];      //title of triangle
	MY_POINT center;  //vertexes
	int radius;
};

void CirclePutData(Circle * ptr, char * title, MY_POINT v1, int radius);
void CirclePrint(void * pdata);
void CircleInput(Circle *ptr);