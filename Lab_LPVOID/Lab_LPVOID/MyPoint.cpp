#include "stdafx.h"
#include "MyPoint.h"
#include <stdlib.h>
#include <string.h>

void PrintVertex(MY_POINT vert)
{
	printf("x = %le   y = %le\n", vert.x, vert.y);
}
