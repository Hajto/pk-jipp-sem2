
#include "stdafx.h"
#include "MyInterf.h"
#include <stdlib.h>
#include <string.h>
#include "MyCircle.h"
#include "MyQuadrangle.h"

void MyMenu()
{
	int ops;
	MY_ARRAY_ELEM *Array = CreateArray(5);

	while (1)
	{
		printf("0 - add object, 1 - print all objects, 2 - exit\n");
		scanf_s("%d", &ops);

		switch (ops)
		{
		case 0:
			AddObject(Array);
			break;
		case 1:
			PrintAllObjects(Array);
			break;
		case 2:
			MyExit(Array);
			break;
		default:
			printf("unknown operation\n");

		};
	}
}

void AddObject(MY_ARRAY_ELEM * arr)
{
	int typ;
	

	printf("enter typ: 0 - triangle, 1 - quadrilateral, 2 - circle\n");
	scanf_s("%d", &typ);
	
	
	switch (typ)
	{
	case 0:
		AddTriangle(arr);
		break;
	case 1:
		AddQuadrangle(arr);
		break;
	case 2:
		AddCircle(arr);
		break;
	default:
		printf("unknown type of object\n");
	};
}

void AddTriangle(MY_ARRAY_ELEM* arr) {
	Triangle *tr;
	tr = (Triangle *)malloc(sizeof(Triangle));  //powstaje obiekt Triangle
	if (!tr)
	{
		printf("memory allocation error\n");
		return;
	}
	TriangInput(tr);

	MY_ARRAY_ELEM tmp;
	tmp.ptr_data = (void *)tr;
	tmp.typ = MY_DATA_TYPE_TRIANG;
	tmp.ptr_fun = TriangPrint;

	if (!PushObject(arr, &tmp)) {
		printf("Push object error\n"); // Obiekt stworzony, nie wypchany do tablicy oraz nie zwolniony
									   //Should I free(tr) here?
		free(tr);
	}
}

void AddCircle(MY_ARRAY_ELEM* arr) {
	Circle* tr2 = (Circle *)malloc(sizeof(Circle));  //powstaje obiekt Triangle
	if (!tr2)
	{
		printf("memory allocation error\n");
		return;
	}
	CircleInput(tr2);

	MY_ARRAY_ELEM tmp2;
	tmp2.ptr_data = (void *)tr2;
	tmp2.typ = MY_DATA_TYPE_CIRCLE;
	tmp2.ptr_fun = CirclePrint;

	if (!PushObject(arr, &tmp2)) {
		printf("Push object error\n"); // Obiekt stworzony, nie wypchany do tablicy oraz nie zwolniony
									   //Should I free(tr) here?
		free(tr2);
	}
}

void AddQuadrangle(MY_ARRAY_ELEM* arr) {
	Quadrangle* tr1 = (Quadrangle *)malloc(sizeof(Quadrangle));  //powstaje obiekt Triangle
	if (!tr1)
	{
		printf("memory allocation error\n");
		return;
	}
	QuadrangleInput(tr1);

	MY_ARRAY_ELEM tmp1;
	tmp1.ptr_data = (void *)tr1;
	tmp1.typ = MY_DATA_TYPE_QUADRILAT;
	tmp1.ptr_fun = QuadranglePrint;

	if (!PushObject(arr, &tmp1)) {
		printf("Push object error\n"); // Obiekt stworzony, nie wypchany do tablicy oraz nie zwolniony
									   //Should I free(tr) here?
		free(tr1);
	}
}

void PrintAllObjects(MY_ARRAY_ELEM * arr)
{
	PrintArray(arr);
}

void MyExit(MY_ARRAY_ELEM * arr)
{
	printf("exit\n");
	FreeArray(&arr);
	system("pause");
	exit(1);
}


