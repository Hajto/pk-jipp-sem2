#include "stdafx.h"
#include "MyTriangle.h"
#include <stdlib.h>
#include <string.h>


void TriangPutData(Triangle *ptr, char *title, MY_POINT v1, MY_POINT v2, MY_POINT v3)
{
	strcpy_s(ptr->str, sizeof(ptr->str), title);
	ptr->vert[0] = v1;
	ptr->vert[1] = v2;
	ptr->vert[2] = v3;
}

void TriangInput(Triangle *ptr)
{
	printf("enter title and three vertexes in format x y\n");
	char str[64];
	MY_POINT v1, v2, v3;
	scanf_s("%s", str, sizeof(str));
	scanf_s("%lf%lf", &v1.x, &v1.y);
	scanf_s("%lf%lf", &v2.x, &v2.y);
	scanf_s("%lf%lf", &v3.x, &v3.y);

	TriangPutData(ptr, str, v1, v2, v3);
}

void TriangPrint(void * pdata)
{
	Triangle *ptr = (Triangle *)pdata;

	printf("Triangle %s\n", ptr->str);

	for (size_t it = 0; it < 3; ++it)
	{
		printf("Vertex 1: ");
		PrintVertex(ptr->vert[it]);
	}
}


