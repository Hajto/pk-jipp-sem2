#pragma once

#include "MyPoint.h"

struct Triangle
{
	char str[64];      //title of triangle
	MY_POINT vert[3];  //vertexes
};


void TriangPutData(Triangle *ptr, char *title, MY_POINT v1, MY_POINT v2, MY_POINT v3);
void TriangPrint(void * pdata);
void TriangInput(Triangle *ptr);