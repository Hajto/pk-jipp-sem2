#include "stdafx.h"
#include "MyCircle.h"
#include "string.h"

void CirclePutData(Circle * ptr, char * title, MY_POINT v1, int radius)
{
	strcpy_s(ptr->str, sizeof(ptr->str), title);
	ptr->center = v1; // Center
	ptr->radius = radius;
}

void CirclePrint(void * pdata)
{
	Circle *ptr = (Circle *)pdata;

	printf("Quadrangle %s\n", ptr->str);
	printf("Center:");
	PrintVertex(ptr->center);
	printf("Radius: %d\n", ptr->radius);
}

void CircleInput(Circle * ptr)
{
	printf("enter title and a vertex in format x y, then radius\n");
	char str[64];
	MY_POINT v1;
	int radius;
	scanf_s("%s", str, sizeof(str));
	scanf_s("%lf%lf", &v1.x, &v1.y);
	scanf_s("%d", &radius);

	CirclePutData(ptr, str, v1, radius);
}
