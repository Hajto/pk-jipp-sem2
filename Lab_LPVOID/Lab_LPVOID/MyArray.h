#pragma once

enum MY_DATA_TYPE
{
	MY_DATA_TYPE_TRIANG,
	MY_DATA_TYPE_QUADRILAT,
	MY_DATA_TYPE_CIRCLE,
	MY_DATA_TYPE_TOT
};

//pointer to function
typedef void(*PrintObject)(void * pdat);

struct MY_ARRAY_ELEM
{
	void *ptr_data;        //pointer to data
	enum MY_DATA_TYPE typ; //typ of figure
	PrintObject ptr_fun;   //wskaznik do funkcji
	static size_t last;    //last position, which is free
};

MY_ARRAY_ELEM * CreateArray(size_t dim);
void FreeArray(MY_ARRAY_ELEM ** arr);

int PushObject(MY_ARRAY_ELEM * arr, MY_ARRAY_ELEM *obj);
void PrintArray(MY_ARRAY_ELEM * arr);
