#include "stdafx.h"
#include "MyQuadrangle.h"
#include "string.h"

void QuadranglePutData(Quadrangle * ptr, char * title, 
	MY_POINT v1, MY_POINT v2, 
	MY_POINT v3, MY_POINT v4)
{
	strcpy_s(ptr->str, sizeof(ptr->str), title);
	ptr->vert[0] = v1;
	ptr->vert[1] = v2;
	ptr->vert[2] = v3;
	ptr->vert[3] = v4;
}

void QuadranglePrint(void * pdata)
{
	Quadrangle *ptr = (Quadrangle *)pdata;

	printf("Quadrangle %s\n", ptr->str);

	for (size_t it = 0; it < 4; ++it)
	{
		printf("Vertex 1: ");
		PrintVertex(ptr->vert[it]);
	}
}

void QuadrangleInput(Quadrangle * ptr)
{
	printf("enter title and four vertexes in format x y\n");
	char str[64];
	MY_POINT v1, v2, v3,v4;
	scanf_s("%s", str, sizeof(str));
	scanf_s("%lf%lf", &v1.x, &v1.y);
	scanf_s("%lf%lf", &v2.x, &v2.y);
	scanf_s("%lf%lf", &v3.x, &v3.y); 
	scanf_s("%lf%lf", &v4.x, &v4.y);

	QuadranglePutData(ptr, str, v1, v2, v3, v4);
}
