#pragma once
#include "MyPoint.h"

struct Quadrangle
{
	char str[64];      //title of triangle
	MY_POINT vert[3];  //vertexes
};

void QuadranglePutData(Quadrangle * ptr, char * title, MY_POINT v1, MY_POINT v2, MY_POINT v3, MY_POINT v4);
void QuadranglePrint(void * pdata);
void QuadrangleInput(Quadrangle *ptr);