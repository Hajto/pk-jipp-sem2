#pragma once

enum MY_MESSAGES
{
	MY_MESS_MEM_ALOC_ERROR,    //0
	MY_MESS_COS_TAM_WARN,	   //1	
	MY_MESS_TOTAL              //ZAWSZE OSTATNI !!!
};

enum MY_DECISION
{
	MY_DECISION_BREAK,			//Przerywamy wykonanie programu
	MY_DECISION_CONTINUE        //Kontynujemy wykonanie
};


enum MY_DECISION my_mess_fun(enum MY_MESSAGES mess);