#include "stdafx.h"
#include "MY_STUDENT.h"
#include "my_mess.h"
#include <stdlib.h>
#include <string.h>
#include "Stack.h"

MY_PTR* Alloc()
{
	MY_PTR* node = (MY_PTR*)malloc(sizeof(MY_PTR));
	node->next = NULL;
	if (!node)
	{
		return NULL;
	}
	return node;
}

MY_PTR *Dealloc(MY_PTR * node)
{
	while (node != NULL)
	{
		if (node)
		{
			node->ptr_free(node->ptr_to_data);     
			node = node->next;
		}
	}
	return node;
}

MY_PTR * Pop(MY_PTR *node)
{
	MY_PTR *next=NULL;
	if (node != NULL)
	{
		next = node->next;
		free(node);
	}
	return next;
}

MY_PTR * Push(MY_PTR *node, void *nowy, FreeData var, PrintData var_wypisz)
{
	MY_PTR *nowszy = Alloc(); // nowszy to wska�nik na szczyt stosu
	nowszy->ptr_to_data = nowy;
	nowszy->next = node;
	nowszy->ptr_free = var; 
	nowszy->ptr_print = var_wypisz;
	return nowszy;
}

void Print(MY_PTR *node)
{
	while (node != NULL)
	{
		node->ptr_print(node->ptr_to_data);                        
		node = node->next;
	}
}

void Find(MY_PTR* tmp)
{
	int search_by_crit = MY_STUDENT_Get_Criterium();
	void* Comparement = MY_STUDENT_Find_Comp_Val(search_by_crit);

	while (tmp)
	{
		if (MY_STUDENT_Compare (tmp->ptr_to_data, Comparement, search_by_crit) == 1)     
		{
			MY_STUDENT_Print(tmp);
		}
		tmp = tmp->next;                                                       
	}
	MY_STUDENT_Free(Comparement);
}

/*void Save(MY_PTR * node, char *filename)
{
	int k=0;
	MY_STUDENT* student;
	while (node != NULL)
	{
		k++;
	}
	size_t it, no_items = k;
	unsigned int no_it = (unsigned int)no_items;
	__int64 filepos = 0;
	__int64 *file_desc = (__int64 *)malloc((no_items + 1) * sizeof(__int64));

	if (!file_desc)
		MyExit(NULL, node, file_desc);

	FILE *pf = fopen(filename, "wb"); 

	if (!pf)
		MyExit(pf, node, file_desc);

	if (fwrite(&no_it, sizeof(unsigned int), 1, pf) != 1)
		MyExit(pf, node, file_desc);

	//rezerwujemy miejsce w pliku dla file_descr
	_fseeki64(pf, (no_items + 1) * sizeof(__int64), SEEK_CUR);
	

	while (node!=NULL)
	{
		
		file_desc[it] = ftell(pf);
		student = (MY_STUDENT*)node->ptr_to_data;
	
		if (fwrite(student, sizeof(MY_STUDENT), 1, pf) != 1)
			MyExit(pf, node, file_desc);

		if (fwrite(student->name, sizeof(char), strlen(student->name), pf) != 1)
			MyExit(pf, node, file_desc);

		node = node->next;
		it++;
	}
	

	file_desc[it] = ftell(pf);   

	_fseeki64(pf, sizeof(unsigned int), SEEK_SET);

	if (fwrite(file_desc, sizeof(__int64), no_items + 1, pf) != no_items + 1)
		MyExit(pf, node, file_desc);

	if (pf)
		fclose(pf);
	pf = NULL;

	if (file_desc)
		free(file_desc);
	file_desc = NULL;
}

void * Read(MY_PTR * node, char *filename)
{
	int counter_items;
	MY_STUDENT *odczyt;
	if (node)
		node = Dealloc(node);

	unsigned int no_items = 0, it, rec;
	__int64 rec_len;
	__int64 *file_desc = NULL;

	FILE *pf = fopen(filename, "rb");

	if (!pf)
		MyExit(pf, node, file_desc);

	if (fread(&no_items, sizeof(unsigned int), 1, pf) != 1)
		MyExit(pf, node, file_desc);

	MY_PTR* read_node =(MY_PTR*)malloc(sizeof(MY_PTR));
	file_desc = (__int64 *)malloc((no_items + 1) * sizeof(__int64));

	if (!read_node || !file_desc)
		MyExit(pf, read_node, file_desc);

	if (fread(file_desc, sizeof(file_desc[0]), no_items + 1, pf) != no_items + 1)
		MyExit(pf, read_node, file_desc);

	while (counter_items < no_items)
	{
		if (fread(odczyt, sizeof(odczyt), 1, pf))
		{
			node=Push(read_node, odczyt, MY_STUDENT_Free, MY_STUDENT_Print);
		}
		counter_items++;
	}

	/*for (it = 0; it < no_items; ++it)
	{
	MY_PTR * Push(MY_PTR *node, void *nowy, FreeData var, PrintData var_wypisz)

		rec = no_items - it - 1;
		rec_len = file_desc[rec + 1] - file_desc[rec];

		_fseeki64(pf, file_desc[rec], SEEK_SET);
		if (fread(&tab[rec], sizeof(tab[rec]), 1, pf) != 1)
			MyExit(pf, tab, file_desc);

		tab[rec].model = (char *)malloc(tab[rec].len * sizeof(char));
		if (!tab[rec].model)
			MyExit(pf, tab, file_desc);

		if (fread(tab[rec].model, sizeof(char), tab[rec].len, pf) != tab[rec].len)
			MyExit(pf, tab, file_desc);
	
	//===========================================================================================================================

	if (file_desc)
		free(file_desc);
	file_desc = NULL;

	if (pf)
		fclose(pf);
	pf = NULL;

	return read_node;
}*/

void MyExit(FILE *pft, MY_PTR *node, __int64 *fdesc)
{
	if (pft)
		fclose(pft);

	Dealloc(node);

	if (fdesc)
		free(fdesc);

	printf("fatal error\n");
	system("pause");
	exit(1);
}