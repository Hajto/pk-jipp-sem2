#include "stdafx.h"
#include "my_mess.h"
#include <stdlib.h>
#include <string.h>

static char *my_message_str_tab[] = {
	"E memory allocation error",	//MY_MESS_MEM_ALOC_ERROR
	"W nastepny komunikat"			//MY_MESS_COS_TAM_WARN
};

MY_DECISION my_mess_fun(enum MY_MESSAGES mess)
/*===================================================================================
Przy podanym numerowaniu enum MY_MESSAGES komunikat mess jest poprostu
numierem wiersza w tablice my_message_str_tab.
RatVal:
MY_DECISION_BREAK    - Przerywamy wykonanie programu
MY_DECISION_CONTINUE - Kontynujemy wykonanie
====================================================================================*/
{
	MY_DECISION retval = MY_DECISION_CONTINUE;

	puts(my_message_str_tab[mess] + 1);

	//Analizujemy komunikat: b��d (przerywamy program) albo komunikat 
	//(drukujemy i kontynujemy dalej)
	if (my_message_str_tab[mess][0] == 'E')
	{
		//to jest b��d. Przerywamy wykonanie programu.
		retval = MY_DECISION_BREAK;
	}

	return retval;
}