#pragma once

struct MY_STUDENT
{
	char *name;
	int year;
	enum KIERUNEK_STUDIOW kierunek;
};

enum KIERUNEK_STUDIOW
{
	KIERUNEK_INFORMATYKA,
	KIERUNEK_FIZYKA_TECHNICZNA,
	KIERUNEK_MATEMATYKA
};

void stdin_clear();

void * MY_STUDENT_Single_Alloc();

void * MY_STUDENT_Get_Student(MY_STUDENT * tmp_student);

char * MY_STUDENT_Get_Name();

int MY_STUDENT_Get_Year();

KIERUNEK_STUDIOW MY_STUDENT_Get_Kierunek();

void MY_STUDENT_Print(void * tmp);

void MY_STUDENT_Free(void * tmp);

int MY_STUDENT_Get_Criterium();

void * MY_STUDENT_Find_Comp_Val(int criterium);

int MY_STUDENT_Compare(void * val_from_node, void * val_for_comp, int criterium);
