#include "stdafx.h"
#include "MY_STUDENT.h"
#include "my_mess.h"
#include <stdlib.h>
#include <string.h>

void stdin_clear()
{
	int ch;
	while ((ch = getchar()) != '\n')
	{
		printf("%c", ch);
	}
	return;
}

void * MY_STUDENT_Single_Alloc()
{
	MY_STUDENT *tmp_student = (MY_STUDENT*)malloc(sizeof(MY_STUDENT));
	char *array = NULL;
	tmp_student->name = NULL;

	return (void*)tmp_student;
}

void *MY_STUDENT_Get_Student(MY_STUDENT * tmp_student)
{
	char *name = MY_STUDENT_Get_Name();
	int year = MY_STUDENT_Get_Year();
	KIERUNEK_STUDIOW option = MY_STUDENT_Get_Kierunek();

	MY_STUDENT *tmp = (MY_STUDENT*)tmp_student;
	tmp->name = name;
	tmp->year = year;
	tmp->kierunek = option;

	return (void*)(tmp);

}

char* MY_STUDENT_Get_Name()
{
	char student_name[60];
	char *name_arr = NULL;

	printf("Enter name:");
	int name_size = strlen(student_name) + 1;
	name_arr = (char*)malloc(name_size * sizeof(char));
	stdin_clear();
	gets_s(student_name);
	strcpy_s(name_arr, name_size, student_name);
	puts(name_arr);

	return name_arr;
}

int MY_STUDENT_Get_Year()
{
	int year;
	printf("Enter year: ");
	scanf_s("%d", &year);
	stdin_clear();

	return year;
}

KIERUNEK_STUDIOW MY_STUDENT_Get_Kierunek()
{
	int option;
	printf("0 - kierunek informatyka, 1 - kierunek fizyka techniczna, 2 - kierunek matematyka\n");
	printf("Enter your major: ");
	scanf_s("%d", &option);

	while (option != 0 && option != 1 && option != 2)
	{
		printf("Enter your major: ");
		scanf_s("%d", &option);
	}

	return (KIERUNEK_STUDIOW)(option);
}

void MY_STUDENT_Print(void *tmp)
{
	MY_STUDENT *zrzutowane_tmp = (MY_STUDENT *)tmp;
	printf("%s %d %d \n\n", zrzutowane_tmp->name, zrzutowane_tmp->year, zrzutowane_tmp->kierunek);  //do struktury odwo�uje si� kropk�
}                                                                                             // a strza�k� do wska�nika na strukt�r�

void MY_STUDENT_Free(void *tmp)
{
	while (tmp != NULL)
	{
		if (tmp)
		{
			free(tmp);
		}
	}
}

int MY_STUDENT_Get_Criterium()
{
	int criterium;

	printf(" Podaj kryterium: ");
	printf("0 - po imieniu, 1 - po roku, 2 - po kierunku studiow \n");
	stdin_clear();
	scanf_s("%d", &criterium);

	while (criterium != 0 && criterium != 1 && criterium != 2)
	{
		printf(" Podaj kryterium: ");
		scanf_s("%d", &criterium);
	}

	return criterium;
}

void *MY_STUDENT_Find_Comp_Val(int criterium)
{
	MY_STUDENT *tmp_criterium = (MY_STUDENT*)malloc(sizeof(MY_STUDENT));

	switch (criterium)
	{
	case 0:
		tmp_criterium->name = MY_STUDENT_Get_Name();
		break;
	case 1:
		tmp_criterium->year = MY_STUDENT_Get_Year();
		break;
	case 2:
		tmp_criterium->kierunek = MY_STUDENT_Get_Kierunek();
		break;
	}

	return tmp_criterium;
}

int MY_STUDENT_Compare(void *val_from_node, void* val_for_comp, int criterium)
{
	//  ++++++++    ZAK�ADAM, �E POR�WNYWANE PRZEZEMNIE DANE S� TEGO SAMEGO TYPU    ++++++++  //

	int value = 0;
	MY_STUDENT* zrzut_node = (MY_STUDENT*)val_from_node;
	MY_STUDENT* zrzut_comp = (MY_STUDENT*)val_for_comp;

	switch (criterium)
	{
		case 0:
		{
			if (!strcmp(zrzut_node->name, zrzut_comp->name))
				value = 1;
		}
		break;
		case 1:
		{
			if (zrzut_node->year == zrzut_comp->year)
				value = 1;
		}
		break;
		case 2:
		{
			if (zrzut_node->name == zrzut_comp->name)
				value = 1;
		}
		break;
		default:
			value = 0;
		break;
	}

	return value;
}