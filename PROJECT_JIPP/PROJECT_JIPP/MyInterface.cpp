#include "stdafx.h"
#include "Stack.h"
#include <stdlib.h>
#include <string.h>
#include "MyInterface.h"
#include "MY_STUDENT.h"
#include "my_mess.h"

void MyMenu()
{
	int ops = 0;

	MY_PTR * Stack = NULL;
	char name_of_file;
	while (1)
	{
		printf("0 - push object, 1 - pop object, 2 - find object, 3 - save file, 4 - load file, 5  - exit\n");
		scanf_s("%d", &ops);

		switch (ops)
		{
		case 0:
			PushObject(&Stack);
		break;
		case 1:
			PopObject(&Stack);
		break;
		case 2:
			FindObject(Stack);   
		break;
			/*case 3:
				FileSave();   
				break;
			case 4:
				FileLoad();   
				break;
			case 5:
				Exit();
				break;*/
		default:
			printf("unknown operation\n");

		};
	}
}

void PushObject(MY_PTR **node)
{
	void *tmp = MY_STUDENT_Single_Alloc();
	MY_STUDENT *zrzutowane_tmp = (MY_STUDENT *)tmp;
	tmp = MY_STUDENT_Get_Student(zrzutowane_tmp);
	*node = Push(*node, tmp, MY_STUDENT_Free, MY_STUDENT_Print);

	Print(*node);
}

void PopObject(MY_PTR** node)
{
	*node = Pop(*node); 
	Print(*node);
}

void FindObject(MY_PTR* node)
{
	Find(node);
	Print(node);
}
