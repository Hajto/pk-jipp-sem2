#pragma once

typedef void(*PrintData)(void *elem);
typedef void(*FreeData)(void *elem);


struct MY_PTR 

{
	void *ptr_to_data;
	PrintData ptr_print;
	FreeData ptr_free;
	MY_PTR *next;
};

MY_PTR * Alloc();

MY_PTR * Dealloc(MY_PTR * node);

MY_PTR * Pop(MY_PTR * node);

MY_PTR * Push(MY_PTR * node, void * nowy, FreeData var, PrintData var_wypisz);

void Print(MY_PTR * node);

void Find(MY_PTR* tmp);

void MyExit(FILE * pft, MY_PTR * node, __int64 * fdesc);

