#pragma once
#include "Point.h"

struct MY_LINE
{
	MY_NODE *Nodes_line;
};

void Put_to_Line(MY_NODE v1l, MY_NODE v2l, MY_LINE * line);

void Dealloc_line(MY_LINE * line);

void Przypisz_do_lini(MY_LINE * target, MY_LINE * source);

void Wypisz_line(MY_LINE * line);
