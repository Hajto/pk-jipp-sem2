#include "stdafx.h"
#include "MY_STACK.h"
#include "Triangle.h"
#include "Point.h"
#include <stdlib.h>

MY_NODE* Alloc()
{
	MY_NODE* node = (MY_NODE*)malloc(sizeof(MY_NODE));
	if (!node)
	{
		return NULL;
	}
	return node;
}

MY_NODE* Dealloc(MY_NODE* node)
{
	while (node != NULL)
	{
		if (node)
		{
			free(node);
			node = NULL;
		}
	}
}