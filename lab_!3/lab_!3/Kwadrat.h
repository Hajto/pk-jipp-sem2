#pragma once
#include "Point.h"

struct MY_KWADRAT
{
	MY_NODE *Nodes_kwadrat;
};

void Put_to_Kwadrat(MY_NODE v1k, MY_NODE v2k, MY_NODE v3k, MY_NODE v4k, MY_KWADRAT * kwadrat);

void Kwadrat_Dealloc(MY_KWADRAT * kwadrat);

void Przypisz_kwadraty(MY_KWADRAT * source, MY_KWADRAT * target);

void Wypisz_kwadrat(MY_KWADRAT * kwadrat);
