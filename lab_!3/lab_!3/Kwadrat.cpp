#include "stdafx.h"
#include "Point.h"
#include "Kwadrat.h"
#include <string.h>
#include <stdlib.h>

void Put_to_Kwadrat(MY_NODE v1k, MY_NODE v2k, MY_NODE v3k, MY_NODE v4k, MY_KWADRAT *kwadrat)
{
	kwadrat->Nodes_kwadrat = (MY_NODE*)malloc(sizeof(MY_NODE) * 4);
	kwadrat->Nodes_kwadrat[0] = v1k;
	kwadrat->Nodes_kwadrat[1] = v2k;
	kwadrat->Nodes_kwadrat[2] = v3k;
	kwadrat->Nodes_kwadrat[3] = v4k;
}

void Kwadrat_Dealloc(MY_KWADRAT* kwadrat)
{
	if (kwadrat != NULL)
	{
		if (kwadrat->Nodes_kwadrat)
		{
			free(kwadrat->Nodes_kwadrat);
			kwadrat->Nodes_kwadrat = NULL;
		}
	}
}

void Przypisz_kwadraty(MY_KWADRAT *source, MY_KWADRAT *target)
{
	for (int i = 0; i < 4; i++)
	{
		target->Nodes_kwadrat[i].crd[0] = source->Nodes_kwadrat[i].crd[0];
		target->Nodes_kwadrat[i].crd[1] = source->Nodes_kwadrat[i].crd[1];
	}
}

void Wypisz_kwadrat(MY_KWADRAT *kwadrat)
{
	for (int i = 0; i < 4; i++)
	{
		printf("Wsp�lrz�dne wierzcho�ka : \n");
		printf("x= %lf, y= %lf", kwadrat->Nodes_kwadrat[i].crd[0], kwadrat->Nodes_kwadrat[i].crd[1]);
	}
}