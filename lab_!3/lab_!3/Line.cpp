#include "stdafx.h"
#include "Point.h"
#include "Line.h"
#include <stdlib.h>

void Put_to_Line(MY_NODE v1l, MY_NODE v2l, MY_LINE* line)
{
	line->Nodes_line = (MY_NODE*)malloc(sizeof(MY_NODE) * 2);
	line->Nodes_line[0] = v1l;
	line->Nodes_line[1] = v2l;
}

void Dealloc_line(MY_LINE* line)
{
	if (line != NULL)
	{
		if (line->Nodes_line)
		{
			free(line->Nodes_line);
			line->Nodes_line = NULL;
		}
	}
}

void Przypisz_do_lini(MY_LINE* target, MY_LINE* source)
{
	for (int i = 0; i < 2; i++)
	{
		target->Nodes_line[i].crd[0] = source->Nodes_line[i].crd[0];
		target->Nodes_line[i].crd[1] = source->Nodes_line[i].crd[1];
	}
}

void Wypisz_line(MY_LINE *line)
{
	for (int i = 0; i < 2; i++)
	{
		printf("Wsp�lrz�dne wierzcho�ka : \n");
		printf("x= %lf, y= %lf", line->Nodes_line[i].crd[0], line->Nodes_line[i].crd[1]);
	}
}