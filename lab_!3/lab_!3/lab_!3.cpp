// lab_!3.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Point.h"
#include "Triangle.h"
#include "Kwadrat.h"
#include "Line.h"

int main()
{
	//		TR�JK�T
	MY_NODE v1 = { 0, 0 }, v2 = { 1,2 }, v3 = { 2,1 }, v4 = { 1,1 }, v5 = { 2,4 }, v6 = { 3,2 };
	MY_TRIANGLE ABC, MNK;
	Trinagle_Put_Data(v1, v2, v3, &ABC);
	Trinagle_Put_Data(v4, v5, v6, &MNK);
	
	printf("ABC: \n");
	Wypisz(&ABC);
	printf("MNK:\n");
	Wypisz(&MNK);

	Przypisanie(&MNK, &ABC);

	printf("ABC: \n");
	Wypisz(&ABC);
	printf("MNK:\n");
	Wypisz(&MNK);

	Triangle_Dealloc(&ABC);
	Triangle_Dealloc(&MNK);

	//		KWADRAT
	MY_NODE v1k = { 0,0 }, v2k = { 1,1 }, v3k = { 2,2 }, v4k = { 3,3 }, v5k = { 4,4 }, v6k = { 5,5 }, v7k = { 6,6 }, v8k = { 7,7 };
	MY_KWADRAT ABCD, VXYZ;
	Put_to_Kwadrat(v1k, v2k, v3k, v4k, &ABCD);
	Put_to_Kwadrat(v1k, v2k, v3k, v4k, &VXYZ);

	printf("ABCD \n");
	Wypisz_kwadrat(&ABCD);
	printf("VXYZ \n");
	Wypisz_kwadrat(&VXYZ);

	Przypisz_kwadraty(&ABCD, &VXYZ);

	printf("ABCD \n");
	Wypisz_kwadrat(&ABCD);
	printf("VXYZ \n");
	Wypisz_kwadrat(&VXYZ);

	Kwadrat_Dealloc(&ABCD);
	Kwadrat_Dealloc(&VXYZ);

	//		LINIA
	MY_NODE v1l = { 0,0 }, v2l = { 1,1 };
	MY_LINE AB, XY;
	Put_to_Line(v1l, v2l, &AB);
	Put_to_Line(v1l, v2l, &XY);

	printf("AB \n");
	Wypisz_line(&AB);
	printf("XY \n");
	Wypisz_line(&XY);

	Przypisz_do_lini(&AB, &XY);

	printf("AB \n");
	Wypisz_line(&AB);
	printf("XY \n");
	Wypisz_line(&XY);

	Dealloc_line(&AB);
	Dealloc_line(&XY);

	return 0;
}

