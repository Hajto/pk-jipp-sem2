#include "stdafx.h"
#include "Triangle.h"
#include "Point.h"
#include <string.h>
#include <stdlib.h>


void Trinagle_Put_Data(MY_NODE v1, MY_NODE v2, MY_NODE v3, MY_TRIANGLE* tr)
{
	tr->Nodes = (MY_NODE*)malloc(sizeof(MY_NODE) * 3);          //3 bo trojk�t ma 3 wierzcho�ki
	tr->Nodes[0] = v1;
	tr->Nodes[1] = v2;
	tr->Nodes[2] = v3;
}

void Triangle_Dealloc(MY_TRIANGLE* tr)
{
	if (tr != NULL)
	{
		if (tr->Nodes)
		{
			free(tr->Nodes);
			tr->Nodes = NULL;
		}
		free(tr);
		tr = NULL;
	}
}

void Przypisanie(MY_TRIANGLE *source, MY_TRIANGLE *target)
{
	for (int i = 0; i < 3; i++)
	{
		target->Nodes[i].crd[0] = source->Nodes[i].crd[0];			//tu operujemy na wsp�rzednych, nie na wierzcho�kach
		target->Nodes[i].crd[1] = source->Nodes[i].crd[0];
	}
}

void Wypisz(MY_TRIANGLE *tr)
{
	for (int i = 0; i < 3; i++)
	{
		printf("Wsp�lrz�dne wierzcho�ka : \n");
		printf("x= %lf, y= %lf", tr->Nodes[i].crd[0], tr->Nodes[i].crd[1]);
	}
}
